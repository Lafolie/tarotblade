function pupdate(o)
	local x,y=0,0
	if(btn(0))x-=1
	if(btn(1))x+=1
	if(btn(2))y-=3
	if(btn(3))y+=2

	if btn(5) then
		o.w=max(1,o.w+x)
		o.h=max(1,o.h+y)
		if(y>0)o.y-=y
		o.vx=0
		o.vy=0
	else
		o.vx=x
		o.vy=y
	end
end

function player(x,y)
	local p=obj(x,y)
	p.update=pupdate
	return p
end