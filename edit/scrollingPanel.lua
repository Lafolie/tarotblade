require "rect"
require "panel"
require "scrollBar"

--[[
		ScrollingPanel
		A Panel that can display its contents according to a scrolling view.

		
]]

ScrollingPanel = class (Panel)
{
	init = function(self, x, y, w, h, color)
		Panel.init(self, x, y, w, h, color)

		self.consumeWheelInput = true
		self.autoResize = false
		self.contentWidth = 0 --total width of contents
		self.contentHeight = 0
		self.scrollBarSize = 8 --width (vertical) OR height (horizontal) of the bar

		--offsets for the current scroll poisitons
		self.scrollPosX = 0
		self.scrollPosY = 0

		--these vars dictate whether the scrollbars can ever be enabled, not whether they are currently enabled
		--the scrollbars have their own enabled properties for that
		self.canEverScrollHorizontal = true
		self.canEverScrollVertical = true

		self.scrollBarHorizontal = ScrollBar(0, false, 0, self.h - self.scrollBarSize, self.w, self.scrollBarSize)
		self.scrollBarVertical = ScrollBar(0, true, self.w - self.scrollBarSize, 0, self.scrollBarSize, self.h)

		--this rect is displayed when both bars are enabled
		self.showCornerRect = false
		self.scrollCorner = Rect(self.w - self.scrollBarSize, self.h - self.scrollBarSize, self.scrollBarSize, self.scrollBarSize)

		self.onScrollPositionChanged = Event()

		self:updateScrollBars()
		self.onRectAdded:bind(self.updateScrollBars, self)
		self.onResize:bind(self.updateScrollBars, self)
	end,

	wheelMoved = function(self, x, y, wx, wy)
		local rect = self:getRectAtPoint(x, y)
		local finalRect = self:getRectAtPointByPredicate(x, y, function(r) return r.consumeWheelInput end)
		
		--check whether the child node needs the input. If so, continue onward
		if finalRect and finalRect ~= self then
			rect:wheelMoved(x - self.x, y - self.y, wx, wy)
		else
			--otherwise, this input was meant for this panel
			self:setScrollY(self.scrollPosY - wy)
			
		end
	end,

	calcScrollBounds = function(self)
		local w, h = self.w, self.h
		for _, child in ipairs(self.children) do
			w, h = self:getLargestBounds(w, h, child)
		end

		return w, h
	end,

	setScrollPosition = function(self, x, y)
		x = math.max(0, math.min(x, self.contentWidth - self.h))
		y = math.max(0, math.min(y, self.contentHeight - self.h))

		print(y)

		self.scrollPosX = math.floor(x)
		self.scrollPosY = math.floor(y)

		self.scrollBarVertical:setScrollPosition(self.scrollPosY)
		self.scrollBarHorizontal:setScrollPosition(self.scrollPosX)

		self.onScrollPositionChanged()
	end,

	setScrollX = function(self, x)
		self:setScrollPosition(x, self.scrollPosY)
	end,

	setScrollY = function(self, y)
		self:setScrollPosition(self.scrollPosX, y)
	end,

	updateScrollBars = function(self, w, h)
		local w, h = self:calcScrollBounds()

		print "Content Height:"
		print(h)

		if self.canEverScrollVertical and w > self.w then
			--add a bit of extra scroll room to account for the scroll bar
			self.contentHeight = h + self.scrollBarSize
			self.scrollBarVertical.enabled = true
			--self:updateVerticalScroll()
		else
			self.scrollBarVertical.enabled = false
		end

		if self.canEverScrollHorizontal and h > self.h then
			--setup
			self.contentWidth = w + self.scrollBarSize
			self.scrollBarHorizontal.enabled = true
			---self:updateHorizontalScroll()
		else
			self.scrollBarHorizontal.enabled = false
		end

		self:updateVertical()
		self:updateHorizontal()
		--determine whether we need to show the annoying corner rect
		self.showCornerRect = self.scrollBarHorizontal.enabled and self.scrollBarVertical.enabled

	end,

	updateVertical = function(self)
		self.scrollBarVertical:setRange(self.contentHeight)

		--shrink scrollbar if both bars are displayed
		local h = self.scrollBarHorizontal.enabled and self.h - self.scrollBarSize or self.h
		self.scrollBarVertical:resize(self.scrollBarSize, h)
	end,

	updateHorizontal = function(self)
		self.scrollBarHorizontal:setRange(self.contentWidth)

		--shrink scrollbar if both bars are displayed
		local w = self.scrollBarVertical.enabled and self.w - self.scrollBarSize or self.w
		self.scrollBarHorizontal:resize(w, self.scrollBarSize)
	end,
	
	--[[
		 ======================================================
		 ==  QUERY WITH SCROLL OFFSET                        ==
		 ======================================================
	]]

	getRectAtPoint = function(self, x, y)
		local ox, oy = x, y
		x = x - self.x + self.scrollPosX
		y = y - self.y + self.scrollPosY

		--check the last known collision first to reduce iteration cost
		if self.lastHitRect and self.lastHitRect ~= self then
			if self.lastHitRect:collidePoint(x, y) then
				return self.lastHitRect
			end
		end

		--no previous collision/invalidated so find new rect
		for _, rect in ipairs(self.children) do
			if rect:collidePoint(x, y) then
				self.lastHitRect = rect
				return rect
			end
		end

		--no rect found, see if we should return self
		if self:collidePoint(ox, oy) then
			self.lastHitRect = self
			return self
		end

		--no tomato
		self.lastHitRect = false
		return false
	end,

	-- mouseMoved = function(self, x, y, dx, dy)
	-- 	Panel.mouseMoved(self, x + self.scrollPosX, y + self.scrollPosY, dx, dy)
	-- end,

	-- mousePressed = function(self, x, y, button)
	-- 	Panel.mousePressed(self, x - self.scrollPosX, y - self.scrollPosY, button)
	-- end,

	-- mosueReleased = function(self, x, y, button, confirm)
	-- 	Panel.mouseReleased(self, x - self.scrollPosX, y - self.scrollPosY, button, confirm)
	-- end,

	--[[
		======================================================
		==  DRAWING                                         ==
		======================================================
	]]

	draw = function(self)
		local x, y = love.graphics.transformPoint(self.x, self.y)
		local sx, sy, sw, sh = love.graphics.getScissor()
		love.graphics.intersectScissor(x, y, self.w, self.h)
		
		love.graphics.setColor(0, 1, 0, 1)
		love.graphics.rectangle("line", self.x+1, self.y+1, self.w-2, self.h-2)

		love.graphics.push()
		love.graphics.translate(self.x, self.y)

			love.graphics.setColor(1, 1, 1, 1)
			love.graphics.print(tostring(self.scrollPosY), 5, 5)

			love.graphics.setColor(self.drawColor)
			love.graphics.rectangle("fill", 0, 0, self.w, self.h)
			love.graphics.setColor(1, 0, 0, 1)
			love.graphics.rectangle("line", 0, 0, self.w, self.h)
			
			--translate to account for scrolling
			love.graphics.push()
			love.graphics.translate(-self.scrollPosX, -self.scrollPosY)
				for _, child in ipairs(self.children) do
					child:draw()
				end
			love.graphics.pop()

			--draw the scrollbars
			self.scrollBarVertical:draw()
			self.scrollBarHorizontal:draw()

			if self.showCornerRect then
				self.scrollCorner:draw()
			end

		love.graphics.pop()
		love.graphics.setScissor(sx, sy, sw, sh)

	end
}