--[[
	Clickable
	Defines a rect that can be clicked, hovered, etc
	Base class for GUI elements

	Overridable events:
		onPress(x, y, button) - called when the mouse is pressed over the rect
		onRelease(x, y, button) - called when the mouse is released over the rect
		onClick(x, y, button) - called when the rect has been clicked on (pressed, then released while still within the rect)
		onHoverBegin(x, y) - called when the mouse begins hovering over the rect
		onHoverEnd() - called when the mouse stops hovering over the rect
]]

Clickable = class
{
	init = function(self, x, y, w, h)
		self.x = x
		self.y = y
		self.w = w
		self.h = h
	end,

	mouseOver = function(self, mx, my)

	end,

	mouseOff = function(self)
		
	end,

	mouseDown = function(self, mx, my, button)
		if self.onPress then
			self:onPress(mx, my, button)
		end
	end

	mouseUp = function(self, mx, my, button)
		if self.state == "down" then
	end,

	click = function(self, mx, my, button)
		local x = mx-x
		local y = my-y
		if self.onClick then
			self:onClick(x, y, btn)
		end
	end
}