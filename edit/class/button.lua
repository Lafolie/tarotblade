Button = class
{
	init = function(self, x, y, w, h, label, onClick, color)
		self.x = x
		self.y = y
		self.w = w
		self.h = h
		self.label = label or "New Button"
		self.onClick = onClick or function() print "New button presseed" end
		self.color = {0.5, 0.5, 0.5}
	end,

	setLabel = function(self)
		self.text = love.graphics.newText()
	end,

	draw = function(self)
		love.graphics.setColor(self.color)
		love.graphics.rectangle("fill", self.x, self.y, self.w, self.h)

	end
}