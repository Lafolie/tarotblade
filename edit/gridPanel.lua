--[[
	Grid Panel Rect
	Displays children in a scrollable grid.

	Scrolls vertically, based on the width of the panel.
]]

GridPanel = class (Panel)
{
	init = function(self, x, y, w, h, color)
		self.autoResize = false
	
		self.canScroll = false
		-- self.scrollBarPosition = 0
		-- self.scrollBarHeight = 1
		self.scrollBarWidth = 8
		self.grabbedScrollbar = false

		self:setCelSize(200, 16)
		self.celPadx = 4
		self.celPady = 4
		self.gridWidth = 0
		self.gridHeight = 0
		self.grid = {}

		self.scrollPosition = 0
		self.maxScroll = 0
		self.gridDrawOffset = 0
		self.gridDrawMax = 0

		Panel.init(self, x, y, w, h, color)

		self.hoverColor = {0, 1, 0, 0.5}

		self.scrollBar = ScrollBar(self.h, true, self.w - self.scrollBarWidth, 0, self.scrollBarWidth, self.h, {0.75, 0.75, 0.75, 1})
		

		self:recalcGrid()

		self.onResize:bind(self.recalcGrid, self)
		self.onRectAdded:bind(self.recalcGrid, self)
	end,

	mouseMoved = function(self, x, y, dx, dy)
		--adjust according to the scroll position
		Panel.mouseMoved(self, x, y - self.scrollPosition, dx, dy)
	end,

	mousePressed = function(self, x, y, button)
		Panel.mousePressed(self, x, y - self.scrollPosition, button)
	end,

	mouseReleased = function(self, x, y, button, confirm)
		Panel.mouseReleased(self, x, y - self.scrollPosition, button)
	end,

	wheelMoved = function(self, x, y, wx, wy)
		self:scroll(wy)
	end,

	scroll = function(self, delta)
		if self.canScroll then
			--local maxScroll = self.canScroll and self.maxScroll or 0
			self.scrollPosition = math.min(0, math.max(self.maxScroll, self.scrollPosition + delta))

			local offset = math.abs(math.floor(self.scrollPosition / (self.celHeight + self.celPady)))-1
			local max = offset + math.floor(self.h / (self.celHeight + self.celPady))+1

			self.gridDrawOffset = math.max(0, offset)
			self.gridDrawMax = math.min(self.gridHeight, max)

			--calc scrollbar if we need to
				local alpha = self.scrollPosition / (self.maxScroll)-- - self.scrollBarHeight)
				--self.scrollBarPosition = math.floor((self.h - self.scrollBarHeight) * barPos)
				self.scrollBar:setAlpha(alpha)


		--self.scrollBar:setLocation(self.w - self.scrollBarWidth, self.scrollBarPosition)
		--print(self.scrollBarPosition)
		--print(barPos)
		--print(self.maxScroll)
		--print(self.scrollBarHeight)
		end
	end,

	recalcGrid = function(self, w, h)
		self:buildGrid()
		self:calcScrollBounds()
	end,

	setCelSize = function(self, w, h)
		self.celWidth = math.min(w, w - self.scrollBarWidth)
		self.celHeight = h
	end,

	buildGrid = function(self)
		local w = math.max(1, math.floor(self.w / (self.celWidth + self.celPadx)))
		self.grid = {}

		local gw, gh = 0, 0
		for k, child in ipairs(self.children) do
			local x = (k - 1) % w
			local y = math.floor((k - 1) / w)

			if not self.grid[y] then
				self.grid[y] = {}
			end

			self.grid[y][x] = child
			child:setLocation(x * self.celWidth + x * self.celPadx, y * self.celHeight + y * self.celPady)

			gw = x > gw and x or gw
			gh = y > gh and y or gh
		end

		self.gridWidth = gw
		self.gridHeight = gh
	end,

	calcScrollBounds = function(self)
		--update the scrollbar
		self.maxScroll = (-self.gridHeight - 1) * (self.celHeight + self.celPady) + self.h
		self.canScroll = self.maxScroll < 0

		self.scrollBar.enabled = self.canScroll

		local range = (self.gridHeight > 0 and self.gridHeight or 1) * (self.celHeight + self.celPady)
		self.scrollBar:setRange(range)
		self.scrollBar:setHeight(self.h)
		self.scrollBar:setLocation(self.w - self.scrollBarWidth, 0)
		--self.scrollBarHeight = math.max(8, self.h / (gridY / self.h))
		--self.scrollBar:setHeight(self.scrollBarHeight)

		--self.scrollGutter:setHeight(self.h)
		--self.scrollGutter:setLocation(self.w - self.scrollBarWidth, 0)

		self:scroll(0) --to-do: recalc scroll position
	end,

	draw = function(self)
		local sx, sy, sw, sh = love.graphics.getScissor()
		if sx then
			local x = sx + self.x--math.max(sx, self.x)
			local y = sy + self.y--math.max(sy, self.y)
			local w = math.max(0, x + self.w < x + sw and self.w or sw)
			local h = math.max(0, y + self.h < y + sh and self.h or sh)

			--print(string.format("x:%d\ny:%d\nw:%d\nh:%d", x, y, w, h))
			love.graphics.setScissor(x, y, w, h)
			-- print("x:"..x)
			-- print("y:"..y)
			-- print("w:"..w)
			-- print("h:"..h)
		else
			love.graphics.setScissor(self.x, self.y, self.w, self.h)
		end
		love.graphics.push()
		love.graphics.translate(self.x, self.y)
		
			love.graphics.setColor(self.drawColor)
			love.graphics.rectangle("fill", 0, 0, self.w, self.h)
			love.graphics.push()
			love.graphics.translate(0, self.scrollPosition)

				for y = self.gridDrawOffset, self.gridDrawMax do
					for x = 0, self.gridWidth do
						if self.grid[y] and self.grid[y][x] then
							self.grid[y][x]:draw()
						end
					end
				end

			love.graphics.pop()

			--if self.canScroll then
				--self.scrollGutter:draw()
				self.scrollBar:draw()
			--end

			if sx then
				love.graphics.setScissor(sx, sy, sw, sh)
			else
				love.graphics.setScissor()
			end
		love.graphics.pop()
		
	end,
}