class = require "class"
require "event"

require "rect"

print "\nRECT 1\n--------------------"
r1 = Rect(0, 0, 16, 16)
r1.name = "Rect 1"

print "\nRECT 2\n--------------------"
r2 = Rect(0, 0, 16, 16)
r2.name = "Rect 2"
print "\nRESIZE RECT 1 ATTEMPT 1\n--------------------"
print("r1 addr:"..tostring(r1))
print("r2 addr:"..tostring(r2))
r1:resize(32, 32)

print "\nBIND\n--------------------"
r1.onResize:bind(r2.resize, r2)

print "\nRESIZE RECT 1 ATTEMPT 2\n--------------------"
print("r1 addr:"..tostring(r1))
print("r2 addr:"..tostring(r2))
r1:resize(32, 32)