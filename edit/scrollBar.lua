--[[
		ScrollBar
		Displays the overscroll of a panel using a handle that can be grabbed with the mouse

		Properties:
			bool enabled --if false the scrollbar will not appear and can not be interacted with

		Methods:
			setRange(range) --sets the size (in pixels) of the total scrolling area (i.e. the height of a panel plus its overscroll area)
			setPosition(position) --sets the position in relation to the range (0 ~ range)
			setAlpha(alpha) --sets the alpha position of the handle (0 ~ 1)

		Events:
			onHandleMoved() --called when the handle is moved
]]

ScrollBar = class (Rect)
{
	init = function(self, range, isVertical, x, y, w, h, color)
		Rect.init(self, x, y, w, h, {0, 0, 0, 0.5})

		self.isVertical = isVertical
		self.enabled = true

		self.handle = Rect(0, y, w, h, color or {0.75, 0.75, 0.75, 1})
		self.handle.hoverColor = {0.9, 0.9, 0.9, 1}

		self.hitBox = Rect(x - 50, y - 50, w + 100, h + 100) --used when moving scrollbar for convenience
		
		self.range = range
		self.alpha = 0

		self.handleGrabbed = false

		self.onHandleMoved = Event()

		self:setAlpha(0)
	end,

	-- collidePoint = function(self, x, y)
	-- 	print("isGrabbed:"..tostring(self.handleGrabbed))
	-- 	print("collides with bigger box:"..tostring(self.hitBox:collidePoint(x, y)))
	-- 	return self.handleGrabbed and self.hitBox:collidePoint(x, y) or Rect.collidePoint(self, x, y)
	-- end,

	resize = function(self, w, h)
		self.w = w
		self.h = h

		self:setAlpha(self.alpha) --to-do recalc alpha pos

		self.onResize()
	end,

	beginHover = function(self, x, y)
		if self.handle:collidePoint(x, y) then
			self.handle:beginHover(x, y)
		end
	end,

	endHover = function(self, x, y)
		self.handle:endHover(x, y)
	end,

	mouseMoved = function(self, x, y, dx, dy)

		local alpha = self:getAlphaAtPoint(x - self.x, y - self.y)
		print(alpha)

		if self.handleGrabbed then
			--print "ddfd"
			self:setAlpha(alpha)
			--self.setAlpha(self.alpha + (self.isVertical and dy or dx))
		end
	end,

	mousePressed = function(self, x, y, button)
		x = x - self.x
		y = y - self.y
		
		if self.handle:collidePoint(x, y) then
			print "dfdfd"
			self.handleGrabbed = true
		else

		end
	end,

	mouseReleased = function(self, x, y, button)
		self.handleGrabbed = false
	end,

	calcHandleSize = function(self)
		local w = self.w
		local h = self.h
		
		if self.isVertical then
			h = math.max(8, self.h / (self.range / self.h))
		else
			w = math.max(8, self.w / (self.range / self.w))
		end

		self.handle:resize(w, h)
	end,

	setRange = function(self, range)
		self.range = range
		self:calcHandleSize()
	end,

	setScrollPosition = function(self, position)
		local x = 0
		local y = 0

		if self.isVertical then
			--y = math.max(0, math.min(position, self.h - self.handle.h))
			y = position * self.h / self.range
		else
			x = position * self.h / self.range
		end

		self.handle:setLocation(x, y)
	end,

	getAlphaAtPoint = function(self, x, y)
		return math.max(0, math.min(1, y / (self.h - self.handle.h)))
	end,

	setAlpha = function(self, alpha)
		local x = 0
		local y = 0

		if self.isVertical then
			y = math.floor((self.h - self.handle.h) * alpha)
		else
			x = math.floor((self.w - self.handle.w) * alpha)
		end
		self.alpha = alpha
		self.handle:setLocation(x, y)
	end,

	draw = function(self)
		if self.enabled then
			love.graphics.push()
			love.graphics.translate(self.x, self.y)
			love.graphics.setColor(self.drawColor)
			love.graphics.rectangle("fill", 0, 0, self.w, self.h)

			self.handle:draw()
			love.graphics.pop()
		end
	end
}