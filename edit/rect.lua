--[[
	Rect
	Base UI class. 

	The Rect class its children make use of the Event class a lot.
	Be careful when using Event:clear() as subclasses of Rect may use event bindings for functionality (e.g. CheckBox)

	XY co-ordinates passed to events are relative to the Rect. No absolute co-ords are stored.

	Events:
		onLocationUpdated()
		onResize()
		onMouseDown(x, y, button)
		onMouseUp(x, y, button, confirm)

]]

Rect = class
{
	init = function(self, x, y, w, h, color, hoverColor)
		self.x = x
		self.y = y
		self.w = w
		self.h = h

		self.isHovered = false
		self.consumeWheelInput = false

		self.onLocationUpdated = Event()
		self.onResize = Event()
		self.onMouseMoved = Event()
		self.onMouseDown = Event()
		self.onMouseUp = Event()

		self.color = color or {0.5, 0.5, 0.5, 0.5}
		self.hoverColor = hoverColor or {1, 1, 1, 0.5}--self.color

		self.drawColor = self.color
	end,

	setLocation = function(self, x, y)
		self.x = math.floor(x)
		self.y = math.floor(y)

		self.onLocationUpdated(x, y)
	end,

	setWidth = function(self, w)
		self.w = math.floor(w)

		self.onResize(self.w, self.h)
	end,

	setHeight = function(self, h)
		self.h = math.floor(h)

		self.onResize(self.w, self.h)
	end,

	resize = function(self, w, h)
		self.w = math.floor(w)
		self.h = math.floor(h)

		self.onResize(self.w, self.h)
		--assert(false)
	end,

	-- getRectAtPoint = function(self, x, y)
	-- 	return self:collidePoint(x, y) and self or nil
	-- end,

	collidePoint = function(self, x, y)
		return x > self.x and x < self.x + self.w and y > self.y and y < self.y + self.h
	end,

	beginHover = function(self)
		print("begin hover for rect:", tostring(self))
		self.isHovered = true
		self.drawColor = self.hoverColor
	end,

	endHover = function(self)
		print("end hover for rect:", tostring(self))
		self.isHovered = false
		self.drawColor = self.color
	end,

	mouseMoved = function(self, x, y, dx, dy)
		-- x = x - self.x
		-- y = y - self.y
		local withinBounds = self:collidePoint(x, y)
		if withinBounds and not self.isHovered then
			-- if not self.isHovered then
				self:beginHover()
			-- end
		elseif not (withinBounds and self.isHovered) then
			self:endHover()
		end


	end,

	mousePressed = function(self, x, y, button)
		x = x - self.x
		y = y - self.y
		self.onMouseDown(x, y, button)
	end,

	mouseReleased = function(self, x, y, button, confirm)
		x = x - self.x
		y = y - self.y
		self.onMouseUp(x, y, button, confirm)

		if confirm then
			print(self)
		else
			print("NO CONFIRM FOR: "..tostring(self))
		end
	end,

	wheelMoved = function(self, x, y, wx, wy)

	end,

	draw = function(self)
		love.graphics.setColor(self.drawColor)
		love.graphics.rectangle("fill", self.x, self.y, self.w, self.h)

		love.graphics.setColor(1, 1, 1, 1)
		love.graphics.print(tostring(self), self.x, self.y)--love.graphics.transformPoint(5, 5))
	end
}