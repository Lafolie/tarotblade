require "rect"
require "panel"

Editor = class
{
	init = function(self, x)
		print "EDITOR--------------"
		local w, h = love.graphics.getDimensions()
		self.masterPanel = Panel(x or 0, 0, w - (x or 0), h)
		self.rectUnderCursor = false
		self.pressedRect = false

		self.title = "New Editor"

		--events
		print "declaring events"
		print "1"
		self.onEnter = Event()
		print "2"
		self.onLeave = Event()
		print "3"
		self.onResize = Event()

	end,

	setX = function(self, x)
		local w = love.graphics.getWidth()
		self.masterPanel:setLocation(x, 0)
		self.masterPanel:setWidth(w - x)
		--self.masterPanel.x = x
		--self.masterPanel.w = w - x
		print(w - x)
	end,

	enter = function(self)
		love.window.setTitle(stirng.format("%s - CastleEdit"), self.title)

		self.onEnter()
	end,

	leave = function(self)
		self.onLeave()
	end,

	resize = function(self, w, h)
		self.masterPanel:resize(w - self.masterPanel.x, h)
		-- self.masterPanel.w = w - self.masterPanel.x
		-- self.masterPanel.h = h

		self.onResize(self.masterPanel.w, self.masterPanel.h)
	end,

	update = function(self, dt)
		local x, y = love.mouse.getPosition()
		
		-- --handle rect hovering
		-- local rect = self.masterPanel:getRectAtPoint(x, y) --will always return a table
		-- if rect then
		-- 	if self.rectUnderCursor and rect == self.rectUnderCursor then

		-- 	else
		-- 		if self.rectUnderCursor then
		-- 			self.rectUnderCursor:endHover()
		-- 		end
		-- 		rect:beginHover()
		-- 		self.rectUnderCursor = rect
		-- 	end
		-- else
		-- 	if self.rectUnderCursor then
		-- 		self.rectUnderCursor:endHover()
		-- 	end
		-- 	self.rectUnderCursor = false
		-- end
	end,

	mouseMoved = function(self, x, y, dx, dy)
		self.masterPanel:mouseMoved(x, y, dx, dy)
		-- handle rect hovering
		-- local rect = self.masterPanel:getRectAtPoint(x, y) 
		-- if rect then
		-- 	if self.rectUnderCursor and rect == self.rectUnderCursor then

		-- 	else
		-- 		if self.rectUnderCursor then
		-- 			self.rectUnderCursor:endHover(x, y)
		-- 		end
		-- 		rect:beginHover(x, y)
		-- 		self.rectUnderCursor = rect
		-- 	end
		-- else
		-- 	if self.rectUnderCursor then
		-- 		self.rectUnderCursor:endHover(x, y)
		-- 	end
		-- 	self.rectUnderCursor = false
		-- end

		-- if self.pressedRect then
		-- 	--self.pressedRect:mouseMoved(x, y, dx, dy)
		-- end

		-- if self.rectUnderCursor then
		-- 	--self.rectUnderCursor:mouseMoved(x, y, dx, dy)
		-- end
		-- self.masterPanel:mouseMoved(x, y, dx, dy)
		-- print "here as well"
	end,

	mousePressed = function(self, x, y, button)
		self.masterPanel:mousePressed(x, y, button)
		-- self.pressedRect = self.rectUnderCursor
		-- self.masterPanel:mousePressed(x, y, button)

		-- if self.pressedRect then
		-- 	self.pressedRect:mousePressed(x, y, button)
		-- end
	end,

	mouseReleased = function(self, x, y, button)
		self.masterPanel:mouseReleased(x, y, button)
		-- local confirm = self.rectUnderCursor and self.rectUnderCursor == self.pressedRect
		-- print(confirm)
		-- self.masterPanel:mouseReleased(x, y, button, confirm)
		-- if not confirm and self.pressedRect then
		-- 	self.pressedRect:mouseReleased(x, y, button, confirm)
		-- end
		-- self.pressedRect = false




		-- if self.pressedRect then
		-- 	local confirm = self.rectUnderCursor and self.rectUnderCursor == self.pressedRect

		-- 	self.pressedRect:mouseReleased(x, y, button, confirm)
		-- 	self.pressedRect = false
		-- end
	end,

	wheelMoved = function(self, x, y, wx, wy)
		self.masterPanel:wheelMoved(x, y, wx, wy)
	end,

	keyPressed = function(self, x, y, button)

	end,

	keyReleased = function(self, key)

	end,

	textInput = function(self, text)

	end,

	draw = function(self)
		self.masterPanel:draw()
	end
}