local pad = 4

OpenedFilesList = class (Panel)
{
	Setup = function(self)
		self.headerLabel = Label("Open Files", 0, 0, self.w, 20)
		self.fileGrid = GridPanel(pad, 20 + pad, self.w - pad, self.h - 20 - pad)
		self.fileGrid.name = "grid"
		self:addMultiple(self.headerLabel, self.fileGrid)

		for n = 1, 160 do
			self.fileGrid:addRect(Label(n, 0, 0, 16, 16))
		end

		self.onResize:bind(self.resizeDelegate, self)
	end,

	resizeDelegate = function(self, w, h)
		self.headerLabel:setWidth(w)
		self.fileGrid:resize(w - pad, h - 20 - pad)
	end
}