require "contentBrowser.directoryView"
require "contentBrowser.openedFilesList"

ContentBrowser = class (Editor)
{
	init = function(self, x)
		Editor.init(self, 0)
		--self.masterPanel:resize(x, self.masterPanel.h)
		self.openList = OpenedFilesList(0, 0, x, self.masterPanel.h / 2)
		self.dirView = DirectoryView(0, self.masterPanel.h / 2, x, self.masterPanel.h / 2)
		
		print "GRID ADDRESS:"
		print(self.openList)
		-- self.masterPanel:addMultiple(self.openList, self.dirView)
		self.masterPanel:addRect(self.openList)
		-- local function resize(t, w, h)
		-- 	t:setLocation(0, )
		-- 	t:resize(w, h / 2)
		-- 	print "hi"
		-- end
		self.onResize:bind(self.resizeDelegate, self)

		self:resize(x, self.masterPanel.h)

	end,

	resizeDelegate = function(self, w, h)
		self.openList:resize(w, h * 0.5)
		self.openList:setLocation(0, 0)

		-- self.dirView:resize(w, h * 0.5)
		-- self.dirView:setLocation(0, h * 0.5)
	end,

	-- mouseMoved = function(self, x, y, dx, dy)
	-- 	--self.dirView.fileGrid:mouseMoved(x, y, dx, dy)
	-- end,

	-- wheelMoved = function(self, x, y, wx, wy)
	-- 	x = x - self.masterPanel.x
	-- 	y = y - self.masterPanel.y

	-- 	if self.openList:collidePoint(x, y) then
	-- 		self.openList.fileGrid:scroll(wy)
	-- 	else
	-- 		self.dirView.fileGrid:scroll(wy)
	-- 	end
	-- end,

	-- onResize = function(self)
	-- 	self.dirView:resize(self.masterPanel.w, self.masterPanel.h)
	-- end,

}