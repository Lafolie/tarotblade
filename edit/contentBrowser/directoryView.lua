local pad = 4

DirectoryView = class (Panel)
{
	Setup = function(self)
		self.pad = 4
		self.dirLabel = Label("Directory", 0, 0, self.w, 20, {1, 1, 1, 1})

		self.fileGrid = GridPanel(pad, 20 + pad, self.w - pad, self.h)-- - 20)

		for n = 1, 160 do
			self.fileGrid:addRect(Label(n, 0, 0, 16, 16))
		end

		self:addMultiple(self.dirLabel, self.fileGrid)
		--print(#self.children)

		self.onResize:bind(function(t, w, h) t:setWidth(w) end, self.dirLabel)
		self.onResize:bind(function(t, w, h) t:setWidth(w - pad) end, self.fileGrid)

	end,
	
	-- onResize = function(self)
	-- 	self.dirLabel:setWidth(self.w)
	-- 	self.fileGrid:setWidth(self.w - pad)
	-- end,
}
