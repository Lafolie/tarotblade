class = require "class"

require "event"
require "rect"
require "panel"
require "scrollBar"
require "scrollingPanel"
require "gridPanel"
require "label"
require "checkBox"
require "editor"

local dirs = 
{
	"contentBrowser",
}

for _, dir in ipairs(dirs) do
	for __, file in ipairs(love.filesystem.getDirectoryItems(dir)) do
		require(string.format("%s.%s", dir, file:sub(1, -5)))
		--print(file:sub(1,-5))
	end
end
--require "mode.test"

--local btn = Button(20, 20, 64, 32, "Hello World")



--[[
	==============FREDS CODE===============
	tmp=s[:8192]
print("__gfx__")
# swap bytes
gfx_data = ""
for i in range(0,len(tmp),2):
    gfx_data = gfx_data + tmp[i+1:i+2] + tmp[i:i+1]
print(re.sub("(.{128})", "\\1\n", gfx_data, 0, re.DOTALL))

map_data=s[8192:]
if len(map_data)>0:
    print("__map__")
    print(re.sub("(.{256})", "\\1\n", map_data, 0, re.DOTALL))
]]


-- --overload type() to handle our classes
-- do
-- 	local _type = type
-- 	function type(v)
-- 		if _type(v) == "table" and v._parent then
-- 			return v._parent
-- 		else
-- 			return _type(v)
-- 		end
-- 	end
-- end
print "boot"
--config
local scrollSpeed = 32

--panels
local browser
local editors,currentEditor={}

--separator
local separatorLocation = 196
local separatorWidth = 4
local dragSeparator = false

--window info
local winWidth, winHeight

local testImg = love.graphics.newImage("gfx/iconCastle.png")
local testImg16 = love.graphics.newImage("gfx/iconCastle16.png")

function love.load()
	--init resources
	love.window.setIcon(love.image.newImageData("gfx/iconCastle16.png"))
	local font = love.graphics.newFont("ChronoType.ttf", 16, "none")
	love.graphics.setFont(font)

	love.graphics.setBackgroundColor(0.1, 0.1, 0.125)

	winWidth, winHeight = love.graphics.getDimensions()


	--setup content browser
	browser = ContentBrowser(separatorLocation)

	--masterPanel:add(Filebrowser(0,0,browserWidth,h))
	--masterPanel:add(CurrentEditor)


	--debug prints
	local limits = love.graphics.getSystemLimits()
	print "GFX Limits:\n------------------"
	for k, v in pairs(limits) do
		
		print(string.format("\t%s: %s", k, v))
	end
	
	currentEditor = Editor(separatorLocation + separatorWidth)

	local testScroll = ScrollingPanel(64, 64, 428, 428)
	testScroll:addRect(Rect(396, 396, 64, 64))

	local smallerScroll = ScrollingPanel(48, 48, 128, 128)
	smallerScroll:addRect(Rect(64,100,32,640))
	smallerScroll:addRect(Rect(24,620,32,64))
	testScroll:addRect(smallerScroll)

	currentEditor.masterPanel:addRect(testScroll)
	--currentEditor.masterPanel:shrinkToFitAll()
	
	--currentEditor = ContentBrowser(separatorLocation + separatorWidth)
	table.insert(editors, currentEditor)
end

function love.update(dt)
	
	
	-- browser:update(dt)
	-- currentEditor:update(dt)
end

function love.draw()
	love.graphics.setColor(1, 1, 1, 1)
	--love.graphics.print("Hello world 0x233456ff", 1, 1)
	
	love.graphics.setColor(0.15, 0.15, 0.15)
	love.graphics.rectangle("fill", separatorLocation, 0, separatorWidth, winHeight)

	browser:draw()
	currentEditor:draw()

	love.graphics.setColor(1, 0, 0.3, 1)
	--love.graphics.print(love.timer.getDelta(), 1, 1)
end

-- function love.focus(focus)
-- 	love.update = focus and update or function() end
-- end

function love.handlers.newEditor(editor, file)

end

function love.handlers.switchEditor(index)

end

function love.keypressed(key, scan, isRepeat)

end

function love.keyreleased(key, scan)
	
end

function love.mousemoved(x, y, dx, dy)
	--print "===================BEGIN MOUSEMOVE======================="
	if dragSeparator then
		separatorLocation = math.max(0, math.min(winWidth - separatorWidth, love.mouse.getX()))
		browser:resize(separatorLocation, winHeight)
		for _, editor in ipairs(editors) do
			currentEditor:setX(separatorLocation + separatorWidth)
		end
	end
	-- if x < separatorLocation then
		browser:mouseMoved(x, y, dx, dy)
	-- else
		currentEditor:mouseMoved(x, y, dx, dy)
	-- end
end

function love.mousepressed(x, y, button)
	print "==================BEGIN CLICK==================="
	if x >= separatorLocation + separatorWidth then
		--clicked editor
		currentEditor:mousePressed(x, y, button)
	elseif x >= separatorLocation then
		--clicked separator
		dragSeparator = true
	else
		--clicked browser
		browser:mousePressed(x, y, button)
	end
end

function love.mousereleased(x, y, button)
	browser:mouseReleased(x, y, button)
	currentEditor:mouseReleased(x, y, button)
	dragSeparator = false
end

function love.wheelmoved(wx, wy)
	local x, y = love.mouse.getPosition()
	if love.mouse.getX() < separatorLocation then
		browser:wheelMoved(x, y, wx, wy * scrollSpeed)
	else
		currentEditor:wheelMoved(x, y, wx, wy * scrollSpeed)
	end
end

function love.textinput(text)

end

function love.resize (w, h)
	winWidth = w
	winHeight = h
	if separatorLocation > winWidth then
		separatorLocation = winWidth - separatorWidth
	end

	browser:resize(separatorLocation, h)
	for _, editor in ipairs(editors) do
		editor:setX(separatorLocation + separatorWidth)
		editor:resize(w, h)
	end
end