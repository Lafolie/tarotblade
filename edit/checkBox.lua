--[[
		Checkbox
		Rect that displays a boolean

		Constructor:
			CheckBox(isChecked, x, y, color, hoverColor)

		Properties:
			isChceked			--the boolean value

		Methods:
			toggleChecked() 	--logical NOT the isChecked field
			setChecked(bool)	--sets the value of the bool

		Events:
			onChecked()			--called when the bool is set to true
			onUnchecked()		--called when the bool is set to false
]]

CheckBox = class (Rect)
{
	--these are essentially static properties
	imgTrue = love.graphics.newImage("gfx/checkBox_True.png"),
	imgFalse = love.graphics.newImage("gfx/checkBox_False.png"),

	init = function(self, isChecked, x, y, color, hoverColor)
		color = color or {1, 1, 1, 1}
		self.boxSize = 16

		Rect.init(self, x, y, self.boxSize, self.boxSize, color, color)

		self.isChecked = isChecked

		self.onChecked = Event()
		self.onUnchecked = Event()

		self.onMouseUp:bind(self.clickDelegate, self)

	end,

	clickDelegate = function(self, x, y, button, confirm)
		if confirm then
			self:toggleChecked()	
		end
	end,

	toggleChecked = function(self)
		self:setChecked(not self.isChecked)
	end,

	setChecked = function(self, isChecked)
		self.isChecked = isChecked
		if self.isChecked then
			self.onChecked()
		else
			self.onUnchecked()
		end
	end,

	draw = function(self)
		love.graphics.setColor(self.drawColor)
		if self.isChecked then
			love.graphics.draw(self.imgTrue, self.x, self.y)
		else
			love.graphics.draw(self.imgFalse, self.x, self.y)
		end
	end
}