--[[
	Event
	Multicast event dispatcher.

	Bindings use weak references, so it is safe to destroy objects without explicitly unbinding them, (TEMPORARILY DISABLED)
	however, an unbind() function is available.

	Methods:
		bind(function, table) 	--adds a delegate to the dispatcher list, sending the table as the first argument (i.e. as 'self')
		unbind(function) 		--finds and removes a function from the dispatcher list
		clear() 				--clears all events in the dispatcher list
]]

Event = class
{
	init = function(self)
		self.funcs = {}
	end,

	__call = function(self, ...)
		for n = #self.funcs, 1, -1 do
			local t = self.funcs[n]
			
			if t.owner and t.f then
				
				t.f(t.owner, ...)
				--assert(false)
			else
				table.remove(self.funcs, n)
			end
		end

	end,

	bind = function(self, f, owner)
		--local t = setmetatable({f = f, owner = owner}, {__mode = "v"})
		table.insert(self.funcs, {f = f, owner = owner})
		return true
	end,

	unbind = function(self, f)
		for k, f2 in ipairs(funcs) do
			if f2.f == f then
				table.remove(self.funcs, k)
				return true
			end
		end
	end,

	clear = function(self)
		self.funcs = {}
	end,
}