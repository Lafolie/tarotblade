Label = class (Rect)
{
	init = function(self, str, x, y, w, h, color, textColor)
		Rect.init(self, x, y, w, h, color)

		self.padx = 4
		self.pady = 0
		self.str = str

		self:setLabel(str, textColor)

		self.onLocationUpdated:bind(self.updateTextLocation, self)
		self.onMouseDown:bind(self.debugPrint, self)
	end,

	debugPrint = function(self)
		print(self.str)
	end,

	updateTextLocation = function(self)
		self.textx = self.x + self.padx
		self.texty = math.floor(self.y + (self.h - love.graphics.getFont():getBaseline()) / 2) + self.pady
		
	end,

	setLabel = function(self, str, color)
		self.str = str
		if self.text then
			self.text:set(str)
		else
			self.text = love.graphics.newText(love.graphics.getFont(), str)
		end

		self.textColor = color or {1, 1, 1, 1}

		self:updateTextLocation()
	end,

	draw = function(self)
		love.graphics.setColor(self.drawColor)
		love.graphics.rectangle("fill", self.x, self.y, self.w, self.h)

		love.graphics.setColor(0, 0, 0, 1)
		love.graphics.draw(self.text, self.textx + 1, self.texty + 1)

		love.graphics.setColor(self.textColor)
		love.graphics.draw(self.text, self.textx, self.texty)
	end
}