require "rect"
require "scrollBar"

--[[
	Panel Rect
	Rect capable of holding child rects.

	Properties:
		bool autoResize --automatically set the width/height when child rects are added
		table children --a list of child rects

	Methods:
		addRect(rect)					--adds a child rect
		addMultiple(rect1, rect3, ....) --add multiple child rects

	Callbacks:
		onSetup() --called after initialization, define the layout (children) here

	Events:
		onRectAdded() --called when a rect is added to the panel (todo: pass rect(s)?)
		onRectRemoved() --called when a rect is removed from the panel



]]

Panel = class (Rect)
{
	init = function(self, x, y, w, h, color)
		Rect.init(self, x, y, w, h, (color or {0, 0, 0, 0.25}))

		--BASIC PANEL PROPERTIES--
		--------------------------

		--explicit falsies for clarity
		self.children = {}
		self.lastHitRect = false
		self.hoverRect = false
		self.clickedRect = false
		self.autoResize = false

		--SCROLLING PROPERTIES--
		------------------------

		--basic scroll props
		self.consumeWheelInput = true
		self.contentWidth = 0 --total width of contents
		self.contentHeight = 0
		self.scrollBarSize = 8 --width (vertical) OR height (horizontal) of the bar

		--these vars dictate whether the scrollbars can ever be enabled, not whether they are currently enabled
		--the scrollbars have their own enabled properties for that
		self.canEverScrollHorizontal = true
		self.canEverScrollVertical = true

		--offsets for the current scroll poisitons
		self.scrollPosX = 0
		self.scrollPosY = 0

		--the bastard scrollbars
		self.scrollBarHorizontal = false --ScrollBar(0, false, 0, self.h - self.scrollBarSize, self.w, self.scrollBarSize)
		self.scrollBarVertical = false --ScrollBar(0, true, self.w - self.scrollBarSize, 0, self.scrollBarSize, self.h)
		
		--this rect is displayed when both bars are enabled
		self.showCornerRect = false
		self.scrollCorner = Rect(self.w - self.scrollBarSize, self.h - self.scrollBarSize, self.scrollBarSize, self.scrollBarSize)

		--EVENTS--
		----------

		self.onRectAdded = Event()
		self.onRectRemoved = Event()
		self.onScrollPositionChanged = Event()

		--do setup things
		if self.Setup then
			self:Setup()
		end
	end,

	trySetLocation = function(self, x, y)
		local sx, sy = love.graphics.getDimensions()
		x = math.max(0, x + self.w > sx and sx - self.w or x)
		y = math.max(0, y + self.h > sy and sy - self.h or y)

		self.x = x
		self.y = y
	end,

	--[[
		 ======================================================
		 ==  CHILD ADD / REMOVE                              ==
		 ======================================================
	]]

	addRect = function(self, rect)
		assert(rect:is(Rect), "Non-Rect child added to panel")
		table.insert(self.children, rect)
		
		if self.autoResize then
			self:growToFit(rect)
		end

		self.onRectAdded()
	end,

	addMultiple = function(self, ...)
		for _, rect in ipairs({...}) do
			assert(rect:is(Rect), "Non-Rect child added to panel")
			table.insert(self.children, rect)

			if self.autoResize then
				self:growToFit(rect)
			end
		end

		self.onRectAdded()
	end,

	removeRect = function(self, rect)
		local childIndex
		for index, child in ipairs(self.children) do
			if child == rect then
				childIndex = index
				break
			end
		end

		if childIndex then
			table.remove(self.children, childIndex)
		end

		if self.autoResize then
			self:shrinkToFitAll()
		end

		self.onRectRemoved()
	end,

	removeMultiple = function(self, ...)
		local lut = {}
		for _, r in ipairs({...}) do
			lut[r] = r
		end

		for n = #self.children, 1, -1 do
			local child = self.children[n]
			if lut[child] then
				table.remove(self.children, n)
			end
		end

		if self.autoResize then
			self:shrinkToFitAll()
		end

		self.onRectRemoved()
	end,

	--[[
		 ======================================================
		 ==  GROW / SHRINK FUNCTIONS                         ==
		 ======================================================
	]]

	getLargestBounds = function(self, w, h, child)
		local cw = child.x + child.w
		local ch = child.y + child.h

		w = cw > w and cw or w
		h = ch > h and ch or h

		return w, h
	end,

	growToFit = function(self, child)
		local w, h = self:getLargestBounds(self.w, self.h, child)
		self:resize(w, h)
	end,

	growToFitAll = function(self)
		--initialise to ensure an empty table produces non-nil result
		local w, h = self.w, self.self.h 

		for k, child in ipairs(self.children) do
			w, h = self:getLargestBounds(w, h, child)
		end

		self:resize(w, h)
	end,

	shrinkToFitAll = function(self, child)
		local w, h = 1, 1

		for k, child in ipairs(self.children) do
			w, h = self:getLargestBounds(w, h, child)
		end

		self:resize(w, h)
	end,

	--[[
		 ======================================================
		 ==  SCROLL FUNCTIONS                                ==
		 ======================================================
	]]

	calcScrollBounds = function(self)
		local w, h = self.w, self.h
		for _, child in ipairs(self.children) do
			w, h = self:getLargestBounds(w, h, child)
		end

		return w, h
	end,

	setScrollPosition = function(self, x, y)
		x = math.max(0, math.min(x, self.contentWidth - self.h))
		y = math.max(0, math.min(y, self.contentHeight - self.h))

		print(y)

		self.scrollPosX = math.floor(x)
		self.scrollPosY = math.floor(y)

		if self.scrollBarVertical then
			self.scrollBarVertical:setScrollPosition(self.scrollPosY)
		end

		if self.scrollBarHorizontal then
			self.scrollBarHorizontal:setScrollPosition(self.scrollPosX)
		end

		self.onScrollPositionChanged()
	end,

	setScrollX = function(self, x)
		self:setScrollPosition(x, self.scrollPosY)
	end,

	setScrollY = function(self, y)
		self:setScrollPosition(self.scrollPosX, y)
	end,

	updateScrollBars = function(self, w, h)
		local w, h = self:calcScrollBounds()

		local enableVert = self.canEverScrollVertical and h > self.h
		local enableHori = self.canEverScrollHorizontal and w > self.w

		--add a bit of extra scroll room to account for the opposing scroll bar
		self.contentHeight = h + (enableHori and self.scrollBarSize or 0)
		self.contentWidth = w + (enableVert and self.scrollBarSize or 0)

		if enableVert then
			--instantiate the bar if needed
			if not self.scrollBarVertical then
				self.scrollBarVertical = ScrollBar(0, true, self.w - self.scrollBarSize, 0, self.scrollBarSize, self.h) 
			end

			--offset if other bar is visible
			local offset = enableHori and self.scrollBarSize or 0

			self.scrollBarVertical.enabled = true
			self.scrollBarVertical:setRange(self.contentHeight - offset)
			self.scrollBarVertical:resize(self.scrollBarSize, self.h - offset)

		elseif self.scrollBarVertical then
			self.scrollBarVertical.enabled = false
		end

		if enableHori then
			--instantiate the bar if needed
			if not self.scrollBarHorizontal then
				self.scrollBarHorizontal = ScrollBar(0, false, 0, self.h - self.scrollBarSize, self.w, self.scrollBarSize)
			end

			--offset if other bar is visible
			local offset = enableVert and self.scrollBarSize or 0

			self.scrollBarHorizontal.enabled = true
			self.scrollBarHorizontal:setRange(self.contentWidth - offset)
			self.scrollBarHorizontal:resize(self.w - offset, self.scrollBarSize)
		
		elseif self.scrollBarHorizontal then
			self.scrollBarHorizontal.enabled = false
		end
		
		--determine whether to consume wheel input
		self.consumeWheelInput = enableVert or enableHori

		--determine whether we need to show the annoying corner rect
		self.showCornerRect = enableVert and enableHori

	end,

	updateVertical = function(self, useoffset)
		local range = self.contentHeight
		local height = self.h

		--offset range and height if horizontal scrollbar is enabled
		local offset = useoffset and self.scrollBarSize or 0

		self.scrollBarVertical:setRange(range - offset)
		self.scrollBarVertical:resize(self.scrollBarSize, height - offset)
	end,

	updateHorizontal = function(self, useoffset)
		local range = self.contentWidth
		local width = self.w

		--offset range and width if vertical scrollbar is enabled
		local offset = useoffset and self.scrollBarSize or 0

		self.scrollBarHorizontal:setRange(range - offset)
		self.scrollBarHorizontal:resize(width - offset, self.scrollBarSize)
	end,

	--[[
		 ======================================================
		 ==  MOUSE INTERACTIONS                              ==
		 ======================================================
	]]

	endHover = function(self)
		--panels need to implement their own endHover but not beginHover
		print("ending hover for:"..tostring(self))
		print("\thoverRect is:"..tostring(self.hoverRect))
		if self.hoverRect and self.hoverRect ~= self then
			self.hoverRect:endHover()
		end
			Rect.endHover(self)
			self.hoverRect = false
	end,

	mouseMoved = function(self, x, y, dx, dy)
		if self.name then print "GRID----------------------------------------" end
		--we need both of these
		local oldRect = self.hoverRect
		local rect = self:getRectAtPoint(x, y)
		local relx, rely = x - self.x + self.scrollPosX, y - self.y + self.scrollPosY

		if rect then
			--if we have changed rect, we need to tell the old rect that we've moved away
			if oldRect and oldRect ~= rect then
				if oldRect == self then
					self:endHover()
				else
					oldRect:mouseMoved(relx, rely, dx, dy)
				end
			end

			--callback into the new rect
			if rect == self then
				Rect.mouseMoved(self, x, y, dx, dy)
			else
				rect:mouseMoved(relx, rely, dx, dy)
			end

			self.hoverRect = rect
		else
			--we didn't hit this panel at all, but if we did last frame then we need to tell the old rect we moved away
			if oldRect then
				if oldRect == self then
					self:endHover()
				else
					oldRect:mouseMoved(relx, rely, dx, dy)
				end
			end

			self.hoverRect = false
		end
		if self.name then print "END GRID------------------------------------" end
	end,

	mousePressed = function(self, x, y, button)
		local rect = self:getRectAtPoint(x, y)
		local relx, rely = x - self.x + self.scrollPosX, y - self.y + self.scrollPosY

		if rect then
			if rect == self then
				Rect.mousePressed(self, x, y, button, confirm)
			else
				rect:mousePressed(relx, rely, button, confirm)
			end

			self.clickedRect = rect
		end
	end,

	mouseReleased = function(self, x, y, button, confirm)
		local clickedRect = self.clickedRect
		local rect = self:getRectAtPoint(x, y)
		local relx, rely = x - self.x + self.scrollPosX, y - self.y + self.scrollPosY

		local confirm = (clickedRect and rect) and (clickedRect == rect)

		--set confirm to false if the rect was partially visiable but the cursor lies beyond the panel extents
		confirm = confirm and self:collidePoint(x, y)

		if rect then
			if rect == self then
				Rect.mouseReleased(self, x, y, button, confirm)
			else
				rect:mouseReleased(relx, rely, button, confirm)
			end
		end

		--make sure to call release on the clicked rect
		if not confirm and self.clickedRect then
			if clickedRect == self then
				Rect.mouseReleased(self, x, y, button, false)
			else
				clickedRect:mouseReleased(relx, rely, button, false)
			end
			self.clickedRect:mousePressed(x , y, button, confirm)
		end

		self.clickedRect = false
	end,

	wheelMoved = function(self, x, y, wx, wy)
		local rect = self:getRectAtPoint(x, y)
		local finalRect = self:getRectAtPointByPredicate(x, y, function(r) return r.consumeWheelInput end)
		local relx, rely = x - self.x + self.scrollPosX, y - self.y + self.scrollPosY
		
		--check whether the child node needs the input. If so, continue onward
		if finalRect and finalRect ~= self then
			rect:wheelMoved(relx, rely, wx, wy)
		else
			--otherwise, this input was meant for this panel
			self:setScrollY(self.scrollPosY - wy)
			
		end
	end,

	--[[
		 ======================================================
		 ==  CHILD QUERIES                                   ==
		 ======================================================
	]]

	--get immediate child at relative location
	getRectAtPoint = function(self, x, y)
		local ox, oy = x, y
		x = x - self.x + self.scrollPosX
		y = y - self.y + self.scrollPosY

		--check the last known collision first to reduce iteration cost
		if self.lastHitRect and self.lastHitRect ~= self then
			if self.lastHitRect:collidePoint(x, y) then
				return self.lastHitRect
			end
		end

		--no previous collision/invalidated so find new rect
		for _, rect in ipairs(self.children) do
			if rect:collidePoint(x, y) then
				self.lastHitRect = rect
				return rect
			end
		end

		--no rect found, see if we should return self
		if self:collidePoint(ox, oy) then
			self.lastHitRect = self
			return self
		end

		--no tomato
		self.lastHitRect = false
		return false
	end,

	--get child (recursively) at relative location
	getRectAtPointRecursive = function(self, x, y)
		local rect = self:getRectAtPoint(x, y)
		if rect:is(Panel) and rect ~=self then
			rect = rect:getRectAtPointRecursive(x - self.x, y - self.y)
		end

		return rect
	end,

	--get first child (recursively) that matches predicate
	getRectAtPointByPredicate = function(self, x, y, pred)
		local rect = self:getRectAtPoint(x, y)
		
		--always null check after getRectAtPoint!
		if rect and (not pred(rect)) and rect:is(Panel) and rect ~=self then
			rect = rect:getRectAtPointRecursive(x - self.x, y - self.y)
		end

		--always null check!
		if rect then
			return pred(rect) and rect or false
		end
		
	end,

	--[[
		 ======================================================
		 ==  DRAWING                                         ==
		 ======================================================
	]]

	draw = function(self)
		--translate and scissor to enforce nice panelling
		local x, y = love.graphics.transformPoint(self.x, self.y)
		local sx, sy, sw, sh = love.graphics.getScissor()
		love.graphics.intersectScissor(x, y, self.w, self.h)
		
		love.graphics.setColor(0, 1, 0, 1)
		love.graphics.rectangle("line", self.x+1, self.y+1, self.w-2, self.h-2)

		love.graphics.push()
		love.graphics.translate(self.x, self.y)

			love.graphics.setColor(1, 1, 1, 1)
			love.graphics.print(tostring(self.scrollPosY), 5, 5)

			love.graphics.setColor(self.drawColor)
			love.graphics.rectangle("fill", 0, 0, self.w, self.h)
			love.graphics.setColor(1, 0, 0, 1)
			love.graphics.rectangle("line", 0, 0, self.w, self.h)
			
			--translate to account for scrolling
			love.graphics.push()
			love.graphics.translate(-self.scrollPosX, -self.scrollPosY)
				for _, child in ipairs(self.children) do
					child:draw()
				end
			love.graphics.pop()

			--draw the scrollbars
			if self.scrollBarVertical then
				self.scrollBarVertical:draw()
			end

			if self.scrollBarHorizontal then
				self.scrollBarHorizontal:draw()
			end

			if self.showCornerRect then
				self.scrollCorner:draw()
			end

		love.graphics.pop()
		love.graphics.setScissor(sx, sy, sw, sh)
	end
}