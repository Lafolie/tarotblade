local grav=2

function drawobj(o)
	rectfill(o.x,o.y,o.x+o.w,o.y+o.h,8)
	if dbug then
		local left,right,top,bottom=flr(o.x/8),flr((o.x+o.w)/8),flr(o.y/8),flr((o.y+o.h)/8)
		for y=top,bottom do
			for x=left,right do
				local ox,oy=x*8,y*8
				rect(ox,oy,ox+8,oy+8,15)
			end
		end
	end
end

function updateobj(o)
	o.y+=grav

end

function phys(o)
	local x,y,w,h,vx,vy,air=o.x,o.y,o.w,o.h,o.vx,o.vy,o.air

	--move x
	x+=vx
	local left,right,top,bottom=flr(x/8),flr((x+w)/8),flr(y/8),flr((y+h)/8)
	local hit,hit2,nx,ny,slope
	local useoff=0
	for n=top,bottom do
		hit=fget(mget(left,n))
		if hit==1 then
			--solid wall
			vx=0
			x=left*8+8
		elseif hit>0 then
			slope=true
			--bottom slope
			-- local slopetbl=ramp[hit-1]
			-- local offset
			-- if n==bottom then
			-- 	offset=slopetbl[x-left*8+1]
			-- 	useoff = offset < useoff and offset or useoff
			-- 	y=bottom*8-h+offset-1
			-- else
			-- 	x=left*8+8
			-- end
		end

		hit2=fget(mget(right,n))
		if hit2==1 then
			--solid wall
			vx=0
			x=right*8-w-1
		elseif hit2>0 then
			slope=true
			--bottom slope
			-- if n==bottom then
			-- 	offset=slopetbl[x+w-right*8+1]
			-- 	useoff=offset<useoff and offset or useoff
			-- 	y=bottom*8-h+offset-1
			-- else
			-- 	x=right*8-w-1
			-- end
		end
	end

	--move y
	vy+=grav
	y+=vy
	left,right,top,bottom=flr(x/8),flr((x+w)/8),flr(y/8),flr((y+h)/8)
	hit,hit2=false,false
	local big,landed=not top==bottom
	for n=left,right do
		hit=fget(mget(n,bottom))
		if hit==1 then-- and vy>0 then
			--solid floor
			if big or vy>=0 then
			vy=grav
			y=bottom*8-h-1
			landed=true
			end
		elseif hit>0 then
			slope=true
			--bottom slope
			-- local slopetbl=ramp[hit-1]
			-- local offset
			-- if n==left then
			-- 	offset=slopetbl[x-left*8+1]
			-- elseif n==right then
			-- 	offset=slopetbl[x+w-right*8+1]
			-- else
			-- 	offset=0
			-- end
			-- temp=offset
			-- if offset < useoff then
			-- 	useoff=offset
			-- end
			-- y=bottom*8-h+offset-1
			-- vy=0
		end

		hit2=fget(mget(n,top))
		if hit2==1 then--and vy<0 then
			--solid ceiling
			if big or vy<0 then
			vy=0
			y=top*8+8
			end
		elseif hit2>0 then
			slope=true
		end
	end
	if landed then
		air=false
	else
		air=true
	end
	
	--slope
	if slope then --vy>=0 and 
		left,right,top,bottom=flr(x/8),flr((x+w)/8),flr(y/8),flr((y+h)/8)
		local highest,lowest=0,bottom*8-h-1+8
		local hi,lo
		for n=left, right do
			--floor slopes
			hit=fget(mget(n, bottom))
			if hit>1 and hit<22 then
				hi=true
				local slope,offset=ramp[hit-1],8--bottom*8-h-1+8

				if left==right then
					--we are 1 tile wide
					local a,b=slope[x-left*8+1],slope[x+w-right*8+1]
					offset=a<b and a or b
				elseif n==left then
					offset=slope[x-left*8+1]

				elseif n==right then
					offset=slope[x+w-right*8+1]
					
				-- elseif left==right then
				-- 	--we are 1 tile wide
				-- 	local a,b=slope[x-left*8+1],slope[x+w-right*8+1]
				-- 	offset=a>b and a or b
				end

				local res=bottom*8-h-1+offset
				lowest=res<lowest and res or lowest
			end

			--ceiling slopes
			hit=fget(mget(n,top))
			if hit>21 then
				lo=true
				local slope,offset=ramp[hit-21],8

				if left==right then
					--1 tile wide
					local a,b=slope[x-left*8+1],slope[x+w-right*8+1]
					offset=a<b and a or b
				elseif n==left then
					offset=slope[x-left*8+1]
				elseif n==right then
					offset=slope[x+w-right*8+1]
				end
				local res=top*8+8-offset
				highest=res>highest and res or highest
				--temp=offset
			end
			
		end
		
		-- temp=highest
		--only adjust if we're below the belt
		if hi and vy>=0 and (y>=lowest or not o.air) then
			y=lowest
			air=false
			o.slope=true
			--temp="ramp"
			vy=grav
			temp="DFDFDFDFD"
		--end
		elseif lo and y<highest then
			y=highest
			vy=0
			--temp="no ramp"
		end
	elseif o.slope then
		--leaving slope
		o.slope=false
		if air and vy>0 then
			air=false
			y=bottom*8-1
		end
		--temp=nil
	end

	--if not air then stop() end
	--update
	o.x=x
	o.y=y
	o.vx=vx
	o.vy=vy
	o.air=air
end

function obj(x,y,w,h)
	return
	{
		x=x or 0,
		y=y or 0,
		w=w or 8,
		h=h or 8,
		vx=0,
		vy=0,
		air=false,
		slope=false,
		draw=drawobj,
		phys=phys
	}
end