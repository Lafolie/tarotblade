local ramp={}

--init ramp lut
function mkrmp(t, angle, up)
	local nxt,i,nxti,xx,y=#t	
	for x=1,8*angle do
		i=ceil(x/8)
		nxti=i+nxt
		if(not t[nxti])t[nxti]={}
		xx=x-(i-1)*8
		y=ceil(x/angle)
		t[nxti][xx]=up and 8-y or y-1
	end
end

for n=1,4 do
	mkrmp(ramp,n)
	mkrmp(ramp,n,1)
end

function cpymap()
	local m,spr={}
	for y=0,15 do
		m[y]={}
		for x=0,15 do
			spr=mget(x,y)
			local t={spr=spr,fget(spr)}
			m[y][x]=t
		end
	end
	return m
end

function dumpmap(m)
	for y=0,#m do
		for x=0,#m do
			mset(x,y,m[y][x].spr)
		end
	end
end